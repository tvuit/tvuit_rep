﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BA.Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleCalculator sc = new SimpleCalculator();   // create and instance (object) of our calculator
            Console.WriteLine(sc.Div(4.0, 2.0));            // perform "Div" opertion
            Console.WriteLine(sc.AddWithInc(4.0, 2.0));     // perform "AddWithInc" operation
        }
    }
}
