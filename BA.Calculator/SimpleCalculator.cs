﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BA.Calculator
{
    /// <summary>
    /// Perform simple math action under numbers
    /// </summary>
    public class SimpleCalculator
    {
        /// <summary>
        /// Get result of division operation (n1 / n2)
        /// </summary>
        /// <param name="n1">First number</param>
        /// <param name="n2">Second number</param>
        /// <returns></returns>
        public double Div(double n1, double n2)
        {
            if (n2 == 0.0D)
                throw new DivideByZeroException();
            return n1 / n2;
        }

        /// <summary>
        /// Get result of adding numbers and increment it on one
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <returns></returns>
        public double AddWithInc(double n1, double n2)
        {
            return n1 + n2 + 1;
        }
    }
}